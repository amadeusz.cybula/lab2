package networking.Udp;
 
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

import packet.Packet;
import packet.Spectrum;
import packet.Tools;

public class UdpServer {
    public static void main(String[] args) throws Exception {
    	DatagramSocket aSocket = null;
    	try {
    		aSocket = new DatagramSocket(2137);
    		byte[] buffer = new byte[1024];
    		while(true) {
    			DatagramPacket requestPaczka = new DatagramPacket(buffer, buffer.length);
    			System.out.println("Server:Waiting for request...");
    			aSocket.receive(requestPaczka);     
    			DatagramPacket reply = new DatagramPacket(requestPaczka.getData(), requestPaczka.getLength(), requestPaczka.getAddress(), requestPaczka.getPort());
                aSocket.send(reply);
                Packet read = (Packet) Tools.deserialize(requestPaczka.getData());
                System.out.println(read.toString() + "\n\n");
    			}
    	} catch (SocketException ex) {
    		Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
    	} catch (IOException ex) {
    		Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
    	} finally {
			aSocket.close();
			}
      
    }
}

