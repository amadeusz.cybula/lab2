package networking.Udp; 
import java.io.*;
import java.net.*;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import packet.*;


public class UDPClient {
	public static void main(String[] args) throws Exception{
        DatagramSocket aSocket = null;
        Spectrum spectrum = new Spectrum("Scalowanie","Urzadzenie abc","Opis def",211205);
        Scanner scanner = new Scanner("");
        String line = "";
        try {
            byte[] buffer = new byte[1024];
            InetAddress aHost = InetAddress.getByName("localhost");
            int serverPort = 2137;
            aSocket = new DatagramSocket();
            byte[] data = Tools.serialize(spectrum);
            
            while (true) {
            	System.out.println("Client: Sending datagram");
            	TimeUnit.MILLISECONDS.sleep(2500);
            	if (scanner.hasNextLine())
            		line = scanner.nextLine();				
                DatagramPacket request = new DatagramPacket(data, data.length, aHost, serverPort);
                aSocket.send(request);
                System.out.println("Client: Datagram sent.");
                DatagramPacket getBack = new DatagramPacket(buffer, buffer.length);
                aSocket.receive(getBack); 
                Packet read = (Packet) Tools.deserialize(getBack.getData());
                System.out.println(read.toString() + "\n\n");              
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            aSocket.close();
        }
	}
				

}
