package packet;

public class Alarm extends Packet {
	int channelNr;
	int treshold;
	int direction;
	
	public Alarm(int a,int b,int c){
		channelNr = a;
		treshold = b;
		direction = c;
	}

	@Override
	public String toString() {
		return "Alarm [channelNr=" + channelNr + ", treshold=" + treshold + ", direction=" + direction + "]";
	}
}
