package packet;

import java.util.Arrays;

public class Spectrum extends Sequence {
	String scaling;
	
	public Spectrum(String a,String komp, String opis, long data){
		scaling = a;
		device = komp;
		description = opis;
		date = data;
		
	}

	@Override
	public String toString() {
		return "Spectrum [scaling=" + scaling + ", channelNr=" + channelNr + ", unit=" + unit + ", resolution="
				+ resolution + ", buffer=" + Arrays.toString(buffer) + ", device=" + device + ", description="
				+ description + ", date=" + date + "]";
	}


}
