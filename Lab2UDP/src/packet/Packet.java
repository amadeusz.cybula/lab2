package packet;

import java.io.Serializable;

public abstract class Packet implements Serializable{
	protected String device;
	protected String description;
	protected long date;
	
@Override
public String toString() {
	return "Packet [device=" + device + ", description=" + description + ", date=" + date + "]";
}
}
