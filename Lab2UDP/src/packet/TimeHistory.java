package packet;

public class TimeHistory extends Sequence {
	double sensitivity;
	
	TimeHistory(double a){
		sensitivity = a;
	}

	@Override
	public String toString() {
		return "TimeHistory [sensitivity=" + sensitivity + "]";
	}
}
